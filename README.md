jayaram_linux@cloudshell:~/devfestab (devfestab)$ git clone git@gitlab.com:jayaramk/terraform-gke-simple.git

Cloning into 'terraform-gke-simple'...
remote: Enumerating objects: 4, done.
remote: Counting objects: 100% (4/4), done.
remote: Compressing objects: 100% (4/4), done.
remote: Total 4 (delta 0), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (4/4), done.
```
jayaram_linux@cloudshell:~/devfestab (devfestab)$ ls
terraform-gke-simple
jayaram_linux@cloudshell:~/devfestab (devfestab)$ cd terraform-gke-simple/;ls
simple-gke.tf  variable.tf
```
jayaram_linux@cloudshell:~/devfestab/terraform-gke-simple (devfestab)$ ls -al
total 20
drwxr-xr-x 3 jayaram_linux jayaram_linux 4096 Nov 21 19:25 .
drwxr-xr-x 3 jayaram_linux jayaram_linux 4096 Nov 21 19:25 ..
drwxr-xr-x 8 jayaram_linux jayaram_linux 4096 Nov 21 19:25 .git
-rw-r--r-- 1 jayaram_linux jayaram_linux 1050 Nov 21 19:25 simple-gke.tf
-rw-r--r-- 1 jayaram_linux jayaram_linux  958 Nov 21 19:25 variable.tf


```
jayaram_linux@cloudshell:~/devfestab/terraform-gke-simple (devfestab)$ terraform init
```

Initializing the backend...

Initializing provider plugins...
- Checking for available provider plugins...
- Downloading plugin for provider "google" (hashicorp/google) 3.48.0...

Terraform has been successfully initialized!


You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.

```
jayaram_linux@cloudshell:~/devfestab/terraform-gke-simple (devfestab)$ ls
simple-gke.tf  variable.tf
jayaram_linux@cloudshell:~/devfestab/terraform-gke-simple (devfestab)$ vi variable.tf
jayaram_linux@cloudshell:~/devfestab/terraform-gke-simple (devfestab)$ ls
simple-gke.tf  variable.tf
jayaram_linux@cloudshell:~/devfestab/terraform-gke-simple (devfestab)$ terraform plan
```
Refreshing Terraform state in-memory prior to plan...
The refreshed state will be used to calculate this plan, but will not be
persisted to local or remote state storage.


------------------------------------------------------------------------

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # google_container_cluster.primary will be created
